# %%
import neat
from neat.reporting import BaseReporter
import os
import numpy as np
import matplotlib.pyplot as plt
import visualize
from collections import deque
import wandb

# %%
reward_matrix = np.array([[0,-1],[1,-1000]])
n_rounds_in_fitness = 100
n_rounds_in_eval = 10
nn_input_size = 8

wandb.init(project="gt & nn", name="hawk-dove", config={
    "reward_mat": reward_matrix,
    "n_rounds_in_fitness": n_rounds_in_fitness,
    "n_rounds_in_eval": n_rounds_in_eval,
    "nn_input_size": nn_input_size
})

def simulation(genomes,opponent_list,history,reward_matrix,config, networks):
    rewards = np.zeros(len(genomes))
    results = np.zeros((2,len(genomes)))
    if isinstance(genomes, dict):
        genomes = list(genomes.items())
    for i,((g_id,g),op_id) in enumerate(zip(genomes,opponent_list)):
        g_net = neat.nn.FeedForwardNetwork.create(g, config)
        op_net = neat.nn.FeedForwardNetwork.create(genomes[op_id][1], config)

        g_history = history[:,i]
        op_history = history[:,op_id]

        g_desc = g_net.activate(np.concatenate((g_history,op_history)))[0]
        op_desc = op_net.activate(np.concatenate((op_history,g_history)))[0]

        g_desc = 0 if g_desc < 0.5 else 1
        op_desc = 0 if op_desc < 0.5 else 1

        rewards[i] += reward_matrix[g_desc,op_desc]
        rewards[op_id] += reward_matrix[op_desc,g_desc] 
        results[0,i] = g_desc*2+op_desc 
        results[1,op_id] = op_desc*2+g_desc
        
    return rewards, results

def generate_oppenent_list(n):
    while True:
        opponent_list = np.random.permutation(n)
        if np.all(opponent_list != np.arange(n)):
            break
    return opponent_list


class MatchResultReporter(BaseReporter):
    hists = []
    bins = []
    
    def end_generation(self, config, population, species_set):
        history_size = nn_input_size // 2
        history_deque = deque(maxlen=history_size // 2)
        all_history = []
      
        for _ in range(history_size // 2):
            history_deque.append(np.ones((2, len(population))) * -1)
        
        for i in range(n_rounds_in_eval):
            opponent_list = generate_oppenent_list(len(population))
            _, history = simulation(population,opponent_list,np.concatenate(history_deque),reward_matrix,config)
            history_deque.append(history)
            all_history.append(history)
        all_history = np.array(all_history)
        curr_bins = np.bincount(all_history.flatten().astype(np.int32), minlength=4) / all_history.size
        wandb.log({
            "mutual cooperate": curr_bins[0],
            "cooperate-defect": curr_bins[1] + curr_bins[2],
            "mutual defect": curr_bins[3]
        })
        print(curr_bins)
        self.bins.append(curr_bins)
        self.hists.append(all_history)
    
    def plot_history(self):
        bins_history = np.stack(self.bins, axis=0)
        freq12 = bins_history[:, 1]
        plt.plot(np.arange(len(freq12)), freq12)
        plt.show()
        # plt.figure(figsize=(12, 6))
        # plt.colorbar(label='Game Outcome')
        # plt.xlabel('Generation')
        # plt.ylabel('Genome Index')
        # plt.title('Match History')

# %%
# Determine path to configuration file. This path manipulation is
# here so that the script will run successfully regardless of the
# current working directory.
local_dir = os.getcwd()
config_path = os.path.join(local_dir, 'neat_config')

# Load configuration.
config = neat.Config(neat.DefaultGenome, neat.DefaultReproduction,
                        neat.DefaultSpeciesSet, neat.DefaultStagnation,
                        config_path)

p = neat.Population(config)

match_stats = MatchResultReporter()

# p.add_reporter(neat.StdOutReporter(True))
p.add_reporter(match_stats)
# stats = neat.StatisticsReporter()
# p.add_reporter(stats)


# %%
def fitness(genomes, config):
    rewards = np.zeros(len(genomes))
    history_size = nn_input_size // 2
    history_deque = deque(maxlen=history_size // 2)
    for _ in range(history_size // 2):
        history_deque.append(np.ones((2, len(genomes))) * -1)

    for i in range(n_rounds_in_fitness):
        opponent_list = generate_oppenent_list(len(genomes))
        reward, history = simulation(genomes,opponent_list,np.concatenate(history_deque),reward_matrix,config)
        rewards += reward
        history_deque.append(history)
    for i, (g_id, g) in enumerate(genomes):
        g.fitness = rewards[i]

a = p.run(fitness, 300)  
# %%



