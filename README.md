# Game Theory and Genetic Neural Networks

Many people see neural networks as a mathematical model that can, among other things recreate human-like speech
and sight. Cooperation is not often a feature that is attributed to neural networks. We want to see if given
a set of games, cooperation would emerge from a genetic neural network. To achieve said goal, we want to
recreate games from the seminar and let multiple genetic neural networks run over many iterations to see 
if it displays any behaviours, which have previously not been programmed, such as reaching a Nash equilibrium.