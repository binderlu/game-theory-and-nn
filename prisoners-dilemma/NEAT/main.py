#!/usr/bin/env python
# coding: utf-8

# In[4]:


import neat
from neat.reporting import BaseReporter
import os
import numpy as np
import matplotlib.pyplot as plt
import visualize
from collections import deque
import concurrent.futures


# In[5]:


# %%
reward_matrix = {
    (0, 0): (0, 0),
    (0, 1): (-1, 1),
    (1, 0): (1, -1),
    (1, 1): (-1000, -1000)
}
n_rounds_in_fitness = 10
n_rounds_in_eval = 10
nn_input_size = 8


def simulation(genomes,opponent_list,history,reward_matrix,config, networks):
    rewards = np.zeros(len(genomes))
    results = np.zeros((2,len(genomes)))
    if isinstance(genomes, dict):
        genomes = list(genomes.items())
    with concurrent.futures.ThreadPoolExecutor() as executor:
        futures = []
        for i, ((g_id, g), op_id) in enumerate(zip(genomes, opponent_list)):
            futures.append(executor.submit(single_match, networks[i], networks[op_id], history[:, i], history[:, op_id], reward_matrix, config))
        results = [f.result() for f in concurrent.futures.as_completed(futures)]
    rewards = np.zeros(len(genomes))
    match_results = np.zeros((2, len(genomes)))

    for i, result in enumerate(results):
        rewards[i] += result[0][0]
        rewards[op_id] += result[0][1]
        match_results[0, i] = result[1][0]
        match_results[1, op_id] = result[1][1]

    return rewards, match_results

def single_match(g_net, op_net, g_history, op_history, reward_matrix, config):
    g_desc = g_net.activate(np.concatenate((g_history, op_history)))[0]
    op_desc = op_net.activate(np.concatenate((op_history, g_history)))[0]

    g_desc = 0 if g_desc < 0.5 else 1
    op_desc = 0 if op_desc < 0.5 else 1

    rewards = reward_matrix[(g_desc, op_desc)]
    match_result = [g_desc * 2 + op_desc, op_desc * 2 + g_desc]

    return rewards, match_result

def generate_oppenent_list(n):
    while True:
        opponent_list = np.random.permutation(n)
        if np.all(opponent_list != np.arange(n)):
            break
    return opponent_list


class MatchResultReporter(BaseReporter):
    hists = []
    bins = []
    
    def end_generation(self, config, population, species_set):
        if isinstance(population, dict):
            population = list(population.items())
        history_size = nn_input_size // 2
        history_deque = deque(maxlen=history_size // 2)
        all_history = []
      
        for _ in range(history_size // 2):
            history_deque.append(np.ones((2, len(population))) * -1)
        
        networks = [neat.nn.FeedForwardNetwork.create(g, config) for g_id, g in population]


        for i in range(n_rounds_in_eval):
            opponent_list = generate_oppenent_list(len(population))
            _, history = simulation(population,opponent_list,np.concatenate(history_deque),reward_matrix,config, networks)
            history_deque.append(history)
            all_history.append(history)
        all_history = np.array(all_history)
        curr_bins = np.bincount(all_history.flatten().astype(np.int32), minlength=4) / all_history.size
        print(curr_bins)
        self.bins.append(curr_bins)
        self.hists.append(all_history)
    
    def plot_history(self):
        bins_history = np.stack(self.bins, axis=0)
        freq12 = bins_history[:, 1]
        plt.plot(np.arange(len(freq12)), freq12)
        plt.show()
        # plt.figure(figsize=(12, 6))
        # plt.colorbar(label='Game Outcome')
        # plt.xlabel('Generation')
        # plt.ylabel('Genome Index')
        # plt.title('Match History')

# %%
# Determine path to configuration file. This path manipulation is
# here so that the script will run successfully regardless of the
# current working directory.
local_dir = os.getcwd()
config_path = os.path.join(local_dir, 'neat_config')

# Load configuration.
config = neat.Config(neat.DefaultGenome, neat.DefaultReproduction,
                        neat.DefaultSpeciesSet, neat.DefaultStagnation,
                        config_path)

p = neat.Population(config)

match_stats = MatchResultReporter()

# p.add_reporter(neat.StdOutReporter(True))
p.add_reporter(match_stats)
# stats = neat.StatisticsReporter()
# p.add_reporter(stats)


# %%
def fitness(genomes, config):
    rewards = np.zeros(len(genomes))
    history_size = nn_input_size // 2
    history_deque = deque(maxlen=history_size // 2)


    for _ in range(history_size // 2):
        history_deque.append(np.ones((2, len(genomes))) * -1)

    networks = [neat.nn.FeedForwardNetwork.create(g, config) for g_id, g in genomes]


    for i in range(n_rounds_in_fitness):
        opponent_list = generate_oppenent_list(len(genomes))
        reward, history = simulation(genomes,opponent_list,np.concatenate(history_deque),reward_matrix,config, networks)
        rewards += reward
        history_deque.append(history)
    for i, (g_id, g) in enumerate(genomes):
        g.fitness = rewards[i]




# In[6]:

a = p.run(fitness, 100)
